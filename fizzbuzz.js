console.log("FizzBuzz");




// la function va me permettre d'éviter de répéter mon code
function fizzBuzz() {
    // la variable commence à partie de 0 et s'arrête à 150
    for (let FIZZ = 0; FIZZ <= 150; FIZZ++) {
        // si les chiffres sont des multiples de 3 et 5 afficheront FIZZBUZZ!!
      if (FIZZ % 3 === 0 && FIZZ % 5 === 0) {
        console.log(FIZZ + " FIZZBUZZ!!");
        //  si le chiffre est un multiple de 3 alors FIZZ apparaitra 
      } else if (FIZZ % 3 === 0) {
        console.log(FIZZ + " Fizz");
        //  si le chiffre est un multiple de 5 alors Buzz apparaitra 
      } else if (FIZZ % 5 === 0) {
        console.log(FIZZ + " Buzz");
      } else {
        console.log(FIZZ);
      }
    }
  }
  
  // appel de la fonction
  fizzBuzz();
